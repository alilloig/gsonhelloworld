/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonhelloworld;
 
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class JSONHelloWorld {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    Gson gson;
    File f = null;
    FileReader reader = null;
    FileWriter writer = null;
    Coche coche;
    //Leemos el archivo con 1 coche
    try {
      f = new File("coche.json");
      reader = new FileReader(f);
      gson = new Gson();
      JsonReader jsonReader = new JsonReader(reader);
      coche = gson.fromJson(jsonReader, Coche.class);
      System.out.println("Información de coche.json");
      System.out.println(coche.toString());
    } catch (FileNotFoundException ex) {
      System.err.println("Error al acceder al fichero de configuracion: "+ex.toString());
    } finally {
      try {
        reader.close();
      } catch (IOException ex) {
        System.err.println("Error al cerrar el fichero de configuracion: "+ex.toString());
      } catch (NullPointerException ex){
        System.err.println("El reader nunca fue creado: "+ex.toString());
      }
    }
    
    //Leemos el archivo con varios coches
    try {
      f = new File("coches.json");
      reader = new FileReader(f);
      gson = new Gson();
      JsonReader jsonReader = new JsonReader(reader);
      Coche [] coches = gson.fromJson(jsonReader, Coche[].class);
      System.out.println("Información de cocheS.json");
      for (int i=0;i<coches.length;i++){
        System.out.println(coches[i].toString());
      }
    } catch (FileNotFoundException ex) {
      System.err.println("Error al acceder al fichero de configuracion: "+ex.toString());
    } finally {
      try {
        reader.close();
      } catch (IOException ex) {
        System.err.println("Error al cerrar el fichero de configuracion: "+ex.toString());
      } catch (NullPointerException ex){
        System.err.println("El reader nunca fue creado: "+ex.toString());
      }
    }
    
    //Creamos un archivo nuevo con otro coche
    try {
      f = new File("cocheNuevo.json");
      f.createNewFile();
      coche = new Coche("Volskwagen","Golf", "Gasolina", 1900, 250000, 2010);
      writer = new FileWriter(f);
      gson = new Gson();
      gson.toJson(coche, writer);
      System.out.println("Creado "+coche.toString());
    } catch (FileNotFoundException ex) {
      System.err.println("Error al crear el fichero: "+ex.toString());
    } catch (IOException ex) {
      System.err.println("Error de escritura: "+ex.toString());
    } finally {
      try {
        writer.close();
      } catch (IOException ex) {
        System.err.println("Error al cerrar el fichero de configuracion: "+ex.toString());
      } catch (NullPointerException ex){
        System.err.println("El reader nunca fue creado: "+ex.toString());
      }
    }
  }
  
}
