/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonhelloworld;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Coche {
  private String marca;
  private String modelo;
  private String combustible;
  private int cilindrada;
  private int km;
  private int año;
  
  public Coche (String marca, String modelo, String combustible, int cilindrada, int km, int año){
    this.marca = marca;
    this.modelo = modelo;
    this.año = año;
    this.combustible = combustible;
    this.cilindrada = cilindrada;
    this.km = km;
  }
  
  @Override
  public String toString(){
    return marca + " " + modelo + " " + año;
  }
  
}
